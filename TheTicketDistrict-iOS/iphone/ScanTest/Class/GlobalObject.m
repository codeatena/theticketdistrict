//
//  GlobalObject.m
//  PaymentApp
//
//  Created by JinSung Han on 11/16/13.
//
//

#import "GlobalObject.h"

@implementation GlobalObject

@synthesize qrcodeURL;
@synthesize userID ,userName ,mobileID ,qrCode ,buyer ,seller ,transactID ,status ,amount;
@synthesize  error;


- (void)deleteAllData
{
    self.buyer = @"";
    self.seller = @"";
    self.amount = @"0";
}

@end
