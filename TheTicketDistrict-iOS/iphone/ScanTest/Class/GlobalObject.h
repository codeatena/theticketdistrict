//
//  GlobalObject.h
//  PaymentApp
//
//  Created by JinSung Han on 11/16/13.
//
//

#import <Foundation/Foundation.h>

@interface GlobalObject : NSObject

@property (nonatomic, retain) NSString* qrcodeURL;

@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *userID;
@property (nonatomic, retain) NSString* mobileID;
@property (nonatomic, retain) NSString* qrCode;
@property (nonatomic, retain) NSString* buyer;
@property (nonatomic, retain) NSString* seller;
@property (nonatomic, retain) NSString* transactID;
@property (nonatomic, retain) NSString* status;//Statut
@property (nonatomic, retain) NSString* amount;//amount
@property (readwrite) int   error;

//  an

@property (nonatomic)         BOOL      b_isBuyerSeller;

@property (nonatomic, retain) NSString* receptio1OrReceptionView; // To distinguish Reception1ViewController and ReceptionViewController

- (void)deleteAllData;

@end
