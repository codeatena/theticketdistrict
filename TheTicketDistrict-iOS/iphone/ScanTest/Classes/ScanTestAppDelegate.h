//
//  ScanTestAppDelegate.h
//  ScanTest
//
//  Created by David Kavanagh on 5/10/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GlobalObject;

@class LoginViewController;
@class LogoutViewController;

@class CodeViewController;
@class PaymentViewController;
@class ReceptionViewController;

@class Reception0ViewController;
@class Reception1ViewController;
@class Reception2ViewController;
@class Reception3ViewController;

@class Error3ViewController;
@class Error4ViewController;


@interface ScanTestAppDelegate : NSObject <UIApplicationDelegate> {
    
    UIWindow *window;
    UINavigationController *navigationController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) GlobalObject* globalObject;

- (void)showWaitingScreen:(NSString *)strText bShowText:(BOOL)bShowText;
- (void)hideWaitingScreen;

@end

ScanTestAppDelegate* g_AppDelegate;