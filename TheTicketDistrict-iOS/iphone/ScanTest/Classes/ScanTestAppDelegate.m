// -*- mode:objc; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  Copyright 2010-2012 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import "ScanTestAppDelegate.h"
#import "RootViewController.h"
#import "GlobalObject.h"
#import "Common.h"

@implementation ScanTestAppDelegate

@synthesize window;
@synthesize globalObject;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
  // Override point for customization after app launch
    g_AppDelegate = self;

    NSUUID *uuid = [[UIDevice currentDevice] identifierForVendor];
    g_AppDelegate.globalObject = [[GlobalObject alloc] init];
    //g_AppDelegate.globalObject.mobileID = [NSString stringWithFormat:@"%@", uuid];
    g_AppDelegate.globalObject.mobileID = [uuid UUIDString];
    
    //NSLog(@"%@" ,g_AppDelegate.globalObject.mobileID);
    RootViewController* rootViewCtrl = [[RootViewController alloc] initWithNibName:@"RootViewController" bundle:nil];
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    
   window.rootViewController = rootViewCtrl;
  [window makeKeyAndVisible];
  return YES;
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Save data if appropriate
}

#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	[navigationController release];
	[window release];
	[super dealloc];
}

- (void)showWaitingScreen:(NSString *)strText  bShowText:(BOOL)bShowText {
    
    UIView *view = [[UIView alloc] init];
    
    if ([Common is_Phone]) {
        if ([Common is_Retina]) {
            if ([Common getDeviceType] == DEVICE_TYPE_IPHONE_RETINA_35_INCH) {
                [view setFrame:CGRectMake(0, 0, 320, 480)];
            }
            else {
                [view setFrame:CGRectMake(0, 0, 320, 568)];
            }
        }
        else {
            [view setFrame:CGRectMake(0, 0, 320, 480)];
        }
    }
    else {
        if ([Common is_Retina]) {
            [view setFrame:CGRectMake(0, 0, 768, 1024)];
        }
        else {
            [view setFrame:CGRectMake(0, 0, 768, 1024)];
        }
    }
    
    [view setTag:TAG_WAIT_SCREEN_VIEW];
    [view setBackgroundColor:[UIColor clearColor]];
    [view setAlpha:1.0f];
    
    if (bShowText) {
        UIView *subView = [[UIView alloc] init];
        [subView setBackgroundColor:[UIColor blackColor]];
        [subView setAlpha:0.6];
        
        int width = 0;
        int height = 0;
        
        if ([Common is_Phone]) {
            width = 150;
            height = 100;
        }
        else {
            width = 300;
            height = 200;
        }
        
        [subView setFrame:CGRectMake(view.frame.size.width/2-width/2, view.frame.size.height/2-height/2, width, height)];
        
        UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [indicatorView setTag:TAG_WAIT_SCREEN_INDICATOR];
        
        CGRect rectIndicatorViewFrame = [indicatorView frame];
        
        width = rectIndicatorViewFrame.size.width;
        height = rectIndicatorViewFrame.size.height;
        
        [indicatorView setFrame:CGRectMake(subView.frame.size.width/2-width/2, subView.frame.size.height/3-width/2, width, height)];
        
        [indicatorView startAnimating];
        [subView addSubview:indicatorView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, subView.frame.size.height * 2/3, subView.frame.size.width, subView.frame.size.height/3)];
        [label setText:strText];
        
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        
        [label setTextColor:[UIColor whiteColor]];
        [label setTag:TAG_WAIT_SCREEN_LABEL];
        
        if ([Common is_Phone] ) {
            [label setFont:[UIFont systemFontOfSize:17.0f]];
        }
        else {
            [label setFont:[UIFont systemFontOfSize:34.0f]];
        }
        
        [subView addSubview:label];
        subView = [Common roundCornersOnView:subView onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:10.0f];
        
        [view addSubview:subView];
    }
    
    [self.window addSubview:view];
}

- (void)hideWaitingScreen {
    
    UIView *view = [self.window viewWithTag:TAG_WAIT_SCREEN_VIEW];
    
    if (view) {
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView*)[view viewWithTag:TAG_WAIT_SCREEN_INDICATOR];
        
        if (indicatorView)
            [indicatorView stopAnimating];
        
        [view removeFromSuperview];
        
        UILabel *label = (UILabel *)[view viewWithTag:TAG_WAIT_SCREEN_LABEL];
        if (label) {
            [label removeFromSuperview];
        }
    }
}


@end

