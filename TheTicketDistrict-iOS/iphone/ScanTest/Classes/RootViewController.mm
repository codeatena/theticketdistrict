// -*- Mode: ObjC; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
/*
 * Copyright 2010-2012 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import "RootViewController.h"
#import "MultiFormatReader.h"
#import <ZXingWidgetController.h>
#import <QRCodeReader.h>
#import <TwoDDecoderResult.h>
#import "ScanTestAppDelegate.h"

@implementation RootViewController
@synthesize resultsView;
@synthesize resultsToDisplay;
#pragma mark -
#pragma mark View lifecycle

#define VALIDATE_PATH @"http://tuktukwear.com/ticket/validate.php"

- (void)viewDidLoad {
  [super viewDidLoad];
  [self setTitle:@"ZXing"];
  [resultsView setText:resultsToDisplay];
}

- (IBAction)scanPressed:(id)sender {
	
//    NSURL *url = [NSURL URLWithString:@"http://blogs.extension.org/mastergardener/files/2012/01/sample-QR-code.png"];
//    NSData *data = [NSData dataWithContentsOfURL:url];
//    UIImage *img = [[UIImage alloc] initWithData:data scale:false];
//	
//    //    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
//    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
//    self.readers = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
////    [qrcodeReader release];
//    
//    Decoder *d = [[Decoder alloc] init];
//    [d setDelegate:self];
//    [d setReaders:self.readers];
////    [readers retain];
//    
//    BOOL decodeSuccess= [d decodeImage:img];
//    NSLog(@"BOOL = %@\n", (decodeSuccess ? @"YES" : @"NO"));

    
    ZXingWidgetController *widController =
    [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO showLicense:NO];
    
    NSMutableSet *readers = [[NSMutableSet alloc ] init];
    
    MultiFormatReader* reader = [[MultiFormatReader alloc] init];
    [readers addObject:reader];
    [reader release];
    
    widController.readers = readers;
    [readers release];
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    widController.soundToPlay =
    [NSURL fileURLWithPath:[mainBundle pathForResource:@"beep-beep" ofType:@"aiff"] isDirectory:NO];
    
//  [self presentModalViewController:widController animated:YES];
    [self presentViewController:widController animated:YES completion:nil];

//    widController.overlayView.
    [widController release];
}

- (void)decoder:(Decoder *)decoder didDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset withResult:(TwoDDecoderResult *)result{
    //    [result retain];
    NSLog(@"Did Decode ImDecoderDelegateage Result: %@",[result text]);
    //    [result release];
}

- (void)decoder:(Decoder *)decoder failedToDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset reason:(NSString *)reason
{
    //    [reason retain];
    NSLog(@"Failed Decode Image Result: %@",reason);
    //    [reason release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark ZXingDelegateMethods

- (void)zxingController:(ZXingWidgetController*)controller didScanResult:(NSString *)result {
    
    self.resultsToDisplay = result;
    
    if (self.isViewLoaded) {
        [resultsView setText:resultsToDisplay];
        [resultsView setNeedsDisplay];
        
        [g_AppDelegate showWaitingScreen:@"Validating" bShowText:YES];
        NSString* url = [NSString stringWithFormat:@"%@?code=%@", VALIDATE_PATH, resultsToDisplay];
        
        NSLog(@"validate url = %@", url);
        NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
        NSMutableString *post_data     = [[NSMutableString alloc] init];
        
        [urlRequest setURL:[NSURL URLWithString:url]];
        [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setHTTPMethod:@"GET"];
        [urlRequest setHTTPBody:[post_data dataUsingEncoding:NSUTF8StringEncoding]];
        
        //    NSMutableData *data_users = [[NSMutableData alloc] init]; // add this line in your code
        urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
        
        [btnScan setTitle:@"SCAN NEXT CODE" forState:UIControlStateNormal];
    }
    
//  [self dismissModalViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController*)controller {
//  [self dismissModalViewControllerAnimated:NO];
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidUnload {
  self.resultsView = nil;
}

- (void)dealloc {
  [resultsView release];
  [resultsToDisplay release];
  [super dealloc];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlConnection) {
        
        NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [g_AppDelegate hideWaitingScreen];

        NSLog(@"validate result = %@", myString);
        if ([myString isEqualToString:@"GOOD CODE"]) {
            lblValidateResult.text = @"VALID CODE";
            lblValidateResult.textColor = [UIColor greenColor];
        } else {
            lblValidateResult.text = @"INVALID CODE";
            lblValidateResult.textColor = [UIColor redColor];
        }
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlConnection) {
        // use data from data_users
        
    }
    // do the same for cards connection
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    urlConnection = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}


@end

