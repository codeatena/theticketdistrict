//
//  RootViewController.h
//  ScanTest
//
//  Created by David Kavanagh on 5/10/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZXingWidgetController.h"

@interface RootViewController : UIViewController <ZXingDelegate, DecoderDelegate, NSURLConnectionDelegate> {
    IBOutlet UITextView *resultsView;
    NSString *resultsToDisplay;
    
    NSURLConnection* urlConnection;
    IBOutlet UILabel* lblValidateResult;
    
    IBOutlet UIButton* btnScan;  // test commit
}
@property (nonatomic, retain) IBOutlet UITextView *resultsView;
@property (nonatomic, copy) NSString *resultsToDisplay;

@property (nonatomic, retain) NSSet* readers;
- (void)decoder:(Decoder *)decoder didDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset withResult:(TwoDDecoderResult *)result;
- (void)decoder:(Decoder *)decoder failedToDecodeImage:(UIImage *)image usingSubset:(UIImage *)subset reason:(NSString *)reason;

- (IBAction)scanPressed:(id)sender;
@end
